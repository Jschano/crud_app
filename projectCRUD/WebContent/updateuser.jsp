<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style type="text/css">
#updatefield {
	padding-top: 30px;
	margin-top: 10px;
}
</style>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update User</title>
</head>
<body>
	<sql:setDataSource var="ds" dataSource="jdbc/projectdb" />
	<sql:query dataSource="${ds}" sql="SELECT * FROM userlist" var="result" />

	<h1>Welcome to Update User page</h1>
	<div>
		<table border="2px solid gray">
			<thead>
				<tr>
					<th>Id</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>City</th>
					<th>State</th>
					<th>User Name</th>
					<th>Password</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${result.rows}">
					<tr>
						<td><c:out value="${user.id}" /></td>
						<td><c:out value="${user.firstName}" /></td>
						<td><c:out value="${user.lastName}" /></td>
						<td><c:out value="${user.city}" /></td>
						<td><c:out value="${user.state}" /></td>
						<td><c:out value="${user.userName}" /></td>
						<td><c:out value="${user.password}" /></td>
					</tr>
				</c:forEach>

			</tbody>
		</table>
	</div>
	<div id="updatefield">
		<form method="post"
			action="<%= response.encodeUrl(request.getContextPath() + "/Controller?action=updateuser") %>">
			<input type="hidden" name="action" value="updateuser">
			<table>
				<tr>
					<td>Id number</td>
					<td><input type="text" value="" name="id"></td>
				</tr>
				<tr>
					<td>First name</td>
					<td><input type="text" value="" name="firstName"></td>
				</tr>
				<tr>
					<td>Last name</td>
					<td><input type="text" value="" name="lastName"></td>
				</tr>
				<tr>
					<td>City</td>
					<td><input type="text" value="" name="city"></td>
				</tr>
				<tr>
					<td>State</td>
					<td><input type="text" value="" name="state"></td>
				</tr>
				<tr>
					<td>User Name</td>
					<td><input type="text" value="" name="userName"></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="password" value="" name="password"></td>
				</tr>
				<tr>
					<td>Re-enter Password</td>
					<td><input type="password" value="" name="reenterPassword"></td>
				</tr>

				<tr>
					<td align="center" colspan="2"><input type="submit"
						name="Create User"></td>
				</tr>
			</table>
			</table>
		</form>
	</div>


</body>
</html>