<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create User</title>
</head>
<body>
	<h1>Welcome to Create User page</h1>

	<div>
		<form method="post"
			action="<%= response.encodeURL(request.getContextPath() + "/Controller?action=createuser") %>">
			<table id="addUser">
				<tr>
					<td>First name</td>
					<td><input type="text" value="" name="firstName"></td>
				</tr>
				<tr>
					<td>Last name</td>
					<td><input type="text" value="" name="lastName"></td>
				</tr>
				<tr>
					<td>City</td>
					<td><input type="text" value="" name="city"></td>
				</tr>
				<tr>
					<td>State</td>
					<td><input type="text" value="" name="state"></td>
				</tr>
				<tr>
					<td>User Name</td>
					<td><input type="text" value="" name="userName"></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="password" value="" name="password"></td>
				</tr>
				<tr>
					<td>Re-enter Password</td>
					<td><input type="password" value="" name="reenterPassword"></td>
				</tr>

				<tr>
					<td align="center" colspan="2"><input type="submit"
						name="Create User"></td>
				</tr>
			</table>

			<c:if test="${not empty message}">
				<p class="create-user-error"><%= request.getAttribute("message")%></p>
			</c:if>
		</form>
	</div>
</body>
</html>