<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style text="text/css">
table {
	border: 2px solid black;
}

th, td {
	border-bottom: 1px solid #ddd;
}

tr:hover {
	background-color: #f5f5f5
}
</style>

<title>logged in</title>
</head>
<body>
	<h1>Welcome to the Logged In page</h1>

	<table>
		<thead>
			<tr>
				<td colspan="2"><font color="blue" size="5">Menu
						Selection</font></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Create User</td>
				<td><a href=<%= request.getContextPath() + "/createuser.jsp" %>>new</a>
				</td>
			</tr>

			<tr>
				<td>View Users</td>
				<td><a href=<%= request.getContextPath() + "/viewusers.jsp" %>>view</a>
				</td>
			</tr>
		</tbody>
	</table>

</body>
</html>