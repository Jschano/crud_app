<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<style type="text/css">
html {
	min-height: 100%;
}

#loginpage {
	position: relative;
	top: 40%;
	left: 33%;
	height: 150px;
	width: 300px;
	padding: 40px;
	margin-top: 80px;
	border: 4px solid black;
	background: #75DDFD; /* For browsers that do not support gradients */
	background: -webkit-linear-gradient(#75DDFD, #1A39FE);
	/* For Safari 5.1 to 6.0 */
	background: -o-linear-gradient(#75DDFD, #1A39FE);
	/* For Opera 11.1 to 12.0 */
	background: -moz-linear-gradient(#75DDFD, #1A39FE);
	/* For Firefox 3.6 to 15 */
	background: linear-gradient(#75DDFD, #1A39FE); /* Standard syntax */
}

.login-error {
	font-size: 16px;
	font-weight: bold;
	font-color: red;
}
</style>



<title>Login Page</title>
</head>
<body>
	<div id="loginpage">

		<h3 align="center">Enter Login Info</h3>

		<form method="post"
			action="<%= response.encodeUrl(request.getContextPath() + "/Controller?action=login") %>">

			<input type="hidden" name="action" value="login">

			<table>

				<tr>
					<td align="right">User Name:</td>
					<td><input type="text" name="userName" value=""></td>
				</tr>
				<tr>
					<td align="right">Password:</td>
					<td><input type="password" name="password" value=""></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><input type="submit"
						value="Log in"></td>
				</tr>

			</table>

			<c:if test="${not empty message }">
				<p class="login-error"><%= request.getAttribute("message") %></p>
			</c:if>

		</form>

	</div>

</body>
</html>