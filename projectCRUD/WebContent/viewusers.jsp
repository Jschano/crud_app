<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<sql:setDataSource var="ds" dataSource="jdbc/projectdb" />
	<sql:query dataSource="${ds}" sql="SELECT * FROM userlist" var="result" />

	<div class="userTable" style="overflow-x: auto;">
		<table border="2px gray">
			<thead>
				<tr>
					<th>Id</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>City</th>
					<th>State</th>
					<th>User Name</th>
					<th>Password</th>
					<th colspan="2">Actions</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${result.rows}">
					<tr>
						<td><c:out value="${user.id}" /></td>
						<td><c:out value="${user.firstName}" /></td>
						<td><c:out value="${user.lastName}" /></td>
						<td><c:out value="${user.city}" /></td>
						<td><c:out value="${user.state}" /></td>
						<td><c:out value="${user.userName}" /></td>
						<td><c:out value="${user.password}" /></td>
						<td><a href="Controller.do?action=updateuser">Update</a></td>
						<td><a
							href="Controler.do?action=deleteuser&id=<c:out value='${user.id}'/> ">Delete</a>
						</td>
					</tr>
				</c:forEach>
				<tr>
					<td colspan="9" align="center"><a
						href=<%= request.getContextPath() + "/createuser.jsp" %>>Add
							User</a></td>
				</tr>
			</tbody>
		</table>
	</div>

	<a href=<%= request.getContextPath() + "/loggedin.jsp" %>>Go back
		to selection page</a>

</body>
</html>