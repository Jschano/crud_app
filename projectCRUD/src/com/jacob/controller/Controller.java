package com.jacob.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.jacob.dao.UserDAO;
import com.jacob.user.User;

/** 
 * Servlet implementation class Controller
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DataSource ds;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void init(ServletConfig config) throws ServletException {
		try {

			InitialContext initContext = new InitialContext();

			// set up lookup for jndi Database info for casting from Initial
			// Context to Context
			Context env = (Context) initContext.lookup("java:comp/env");

			// connect datasource to jndi and cast from Context to DataSource
			ds = (DataSource) env.lookup("jdbc/projectdb");

		} catch (NamingException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		String action = request.getParameter("action");

		Connection conn;
		try {
			conn = ds.getConnection();

		} catch (SQLException e) {
			out.println("Unable to connect to database.");
			System.out.println("Unable to connect to database.");
			throw new ServletException();
		}

		UserDAO userDAO = new UserDAO(conn);

		if (action == null) {
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		} else if (action.equals("viewusers")) {
			request.getRequestDispatcher("/viewusers.jsp").forward(request, response);

		} else if (action.equals("deleteuser")) {
			int id = Integer.parseInt(request.getParameter("id"));

			try {
				userDAO.deleteUser(id);
			} catch (SQLException e) {
				e.printStackTrace();
			}

			request.getRequestDispatcher("/deleteuser.jsp").forward(request, response);

		} else if (action.equals("updateuser")) {
			request.getRequestDispatcher("/updateuser.jsp").forward(request, response);

		} else {
			out.println("Unrecognised action");
			return;
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		PrintWriter out = response.getWriter();
		String action = request.getParameter("action");

		if (action == null) {
			out.println("unrecognized action");
			return;
		}

		// get connection
		Connection conn = null;
		try {
			conn = ds.getConnection();

		} catch (SQLException e) {
			out.println("Unable to connect to database.");
			System.out.println("Unable to connect to database.");
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}

		UserDAO userDAO = new UserDAO(conn);

		User user = new User();

		// list of selections
		if (action.equals("login")) {
			String userName = request.getParameter("userName");
			String password = request.getParameter("password");

			request.setAttribute("userName", userName);
			request.setAttribute("password", password);

			try {
				if (userDAO.login(userName, password)) {
					request.getRequestDispatcher("/loggedin.jsp").forward(request, response);
				} else {
					request.setAttribute("message", "User name or Password not correct");
					request.getRequestDispatcher("/login.jsp").forward(request, response);
				}
			} catch (SQLException e) {
				request.setAttribute("userName", "Database error: please try again later.");
				request.getRequestDispatcher("/login.jsp").forward(request, response);
			}

		} else if (action.equals("createuser")) {
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String city = request.getParameter("city");
			String state = request.getParameter("state");
			String userName = request.getParameter("userName");
			String password = request.getParameter("password");
			String reenterPassword = request.getParameter("reenterPassword");

			request.setAttribute("firstName", firstName);
			request.setAttribute("lastName", lastName);
			request.setAttribute("city", city);
			request.setAttribute("state", state);
			request.setAttribute("userName", userName);
			request.setAttribute("password", password);
			request.setAttribute("reenterPassword", reenterPassword);

			if (!password.equals(reenterPassword)) {
				request.setAttribute("message", "Passwords do not match.");
				request.getRequestDispatcher("/createuser.jsp").forward(request, response);
			} else {
				try {
					if (userDAO.userNameExists(userName)) {
						// Checks to verify original user name
						request.setAttribute("message", "Username already used");
						request.getRequestDispatcher("/createuser.jsp").forward(request, response);
					} else {
						// Create new User
						userDAO.createUser(firstName, lastName, city, state, password, userName);
						request.getRequestDispatcher("viewusers.jsp").forward(request, response);
					}

				} catch (SQLException e) {
					request.getRequestDispatcher("/error.jsp").forward(request, response);
				}
			}

		} else if (action.equals("updateuser")) {

			user.setId(Integer.parseInt(request.getParameter("id")));
			
			user.setFirstName(request.getParameter("firstName"));
			user.setLastName(request.getParameter("lastName"));
			user.setCity(request.getParameter("city"));
			user.setState(request.getParameter("state"));
			user.setUserName(request.getParameter("userName"));
			user.setPassword(request.getParameter("password"));
			
			String userName = request.getParameter("userName");
			String password = request.getParameter("password");
			String reenterPassword = request.getParameter("reenterPassword");
			
			request.setAttribute("userName", userName);
			request.setAttribute("password", password);
			request.setAttribute("reenterPassword", reenterPassword);

			if (!password.equals(reenterPassword)) {
				request.setAttribute("message", "Passwords do not match.");
				request.getRequestDispatcher("/updateuser.jsp").forward(request, response);
			} else {
				try {
					if (userDAO.userNameExists(userName)) {
						// Checks to verify original user name
						request.setAttribute("message", "Username already used");
						request.getRequestDispatcher("/updateuser.jsp").forward(request, response);
					} else {
						// Update User
						userDAO.updateUser(user);
						request.getRequestDispatcher("/viewusers.jsp").forward(request, response);
					}

				} catch (SQLException e) {
					request.getRequestDispatcher("/error.jsp").forward(request, response);
				}
			}
		} else {
			out.println("unrecognized action");
		}

		try {
			conn.close();
		} catch (SQLException e) {
			throw new ServletException();
		}
	}

}
