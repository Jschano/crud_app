package com.jacob.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.jacob.user.User;

public class UserDAO {
	
	private Connection conn;
	
	public UserDAO(Connection conn){
		this.conn = conn;
	}
	
	//verify login info
	public boolean login(String userName, String password) throws SQLException{
		String sqlstmt = "SELECT userName, password FROM userlist WHERE userName=? and password=?";
		String dbUsername, dbPassword;
		
		PreparedStatement stmt = conn.prepareStatement(sqlstmt);
		
		stmt.setString(1, userName);
		stmt.setString(2, password);
		
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()){
			dbUsername = rs.getString("userName");
			dbPassword = rs.getString("password");
			
			if(dbUsername.equals(userName) && dbPassword.equals(password)){
				return true;
			}
		}
		
		rs.close();
		
		return false;

	}
	
	//create user to add to userlist database
	public void createUser(String firstName, String lastName, String city, 
			String state, String password, String userName) throws SQLException{
		
		String sqlstmt = "INSERT INTO userlist (firstName, lastName, city, state, password, userName) values (?,?,?,?,?,?)";
		
		PreparedStatement stmt = conn.prepareStatement(sqlstmt);
		
		stmt.setString(1, firstName);
		stmt.setString(2, lastName);
		stmt.setString(3, city);
		stmt.setString(4, state);
		stmt.setString(5, password);
		stmt.setString(6, userName);
		
		stmt.executeUpdate();
		
		stmt.close();		
	}
	
	//delete user from the userlist database
	public void deleteUser(int id) throws SQLException{
		String sqlstmt = "DELETE FROM userlist WHERE id=?";
		String sqlstmt2 = "ALTER TABLE userlist DROP COLUMN id;";
		String sqlstmt3 = "ALTER TABLE userlist ADD id INT PRIMARY KEY AUTO_INCREMENT;";
		
		PreparedStatement stmt = conn.prepareStatement(sqlstmt);
		
		stmt.setInt(1, id);
		
		stmt.executeUpdate();
		
		PreparedStatement stmt2 = conn.prepareStatement(sqlstmt2);
		
		stmt2.executeUpdate();
		
		PreparedStatement stmt3 = conn.prepareStatement(sqlstmt3);
		
		stmt3.executeUpdate();
		
		stmt.close();
	}
	
	//update user info in the userlist database
	public void updateUser(User user) throws SQLException{
		String sqlstmt = "UPDATE userlist SET firstName=?, lastName=?, city=?, state=?, password=?, userName=? where id=?";
		
		PreparedStatement stmt = conn.prepareStatement(sqlstmt);
		
		stmt.setString(1, user.getFirstName());
		stmt.setString(2, user.getLastName());
		stmt.setString(3, user.getCity());
		stmt.setString(4, user.getState());
		stmt.setString(5, user.getPassword());
		stmt.setString(6, user.getUserName());
		stmt.setInt(7, user.getId());
		
		stmt.executeUpdate();
		
		stmt.close();
	}
	
	//get user by specific id number
	/*
	  public User getUserById(int id) throws SQLException{
		User user = new User();
		String sqlstmt = "SELECT * FROM userlist WHERE id=?";
		
		PreparedStatement stmt = conn.prepareStatement(sqlstmt);
		
		stmt.setInt(1, id);
		
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()){
			user.setId(rs.getInt("id"));
			user.setFirstName(rs.getString("firstName"));
			user.setLastName(rs.getString("lastName"));
			user.setCity(rs.getString("city"));
			user.setState(rs.getString("state"));
			user.setPassword(rs.getString("password"));
			user.setUserName(rs.getString("userName"));
		}
				
		rs.close();
		
		stmt.close();
		
		return user;
	}
	*/

	//check to make sure that the User Name has not been used before
	public boolean userNameExists(String username) throws SQLException{
		
		String sqlstmt = "SELECT * FROM userlist where userName=?";
		
		PreparedStatement stmt = conn.prepareStatement(sqlstmt);
		
		stmt.setString(1, username);
		
		ResultSet rs = stmt.executeQuery();
				
		if(rs.next()){
			return true;
		} else{
			return false;
		}
		
	}
	

}
