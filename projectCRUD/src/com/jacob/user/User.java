package com.jacob.user;

public class User {
	
	private int id;
	private String firstName;
	private String lastName;
	private String city;
	private String state;
	private String userName;
	private String password;
	private String message;
	
	public User(){
		
	}
	
	
	public User(int id, String firstName, String lastName, String city, String state, String userName,
			String password) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.city = city;
		this.state = state;
		this.userName = userName;
		this.password = password;
	}
	
	public String getMessage(){
		return message;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean validate(){
		if(firstName == null){
			message = "No First Name entered";
			return false;
		} else if(lastName == null){
			message = "No Last Name entered";
			return false;
		} else if(city == null){
			message = "No City entered";
			return false;
		} else if(state == null){
			message = "No State entered";
			return false;
		} else if(password == null){
			message = "No Password entered";
			return false;
		} else if(userName == null){
			message = "No User Name entered";
			return false;
		} else{
			return true;
		}
	}
	

}
